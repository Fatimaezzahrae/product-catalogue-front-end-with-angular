import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProductModel } from '../model/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor( private http: HttpClient) { }

  getAllProducts(): Observable<ProductModel[]>{
    let host = environment.host;
    return this.http.get<ProductModel[]>(host+"/produit/all");
  }

  getSelectedProducts():Observable<ProductModel[]>{
    let host = environment.host;
    return this.http.get<ProductModel[]>(host+"/produit/chercherSelected?selec=true");
  }

  getAvailabledProducts():Observable<ProductModel[]>{
    let host = environment.host;
    return this.http.get<ProductModel[]>(host+"/produit/chercherAvailable?avail=true");
  }

  searchProducts(keyword: string): Observable<ProductModel[]>{
    let host = environment.host;
    return this.http.get<ProductModel[]>(host+"/produit/chercherByName?name="+keyword);
  }

  select(produit: ProductModel):Observable<ProductModel>{
    let host = environment.host;
    produit.selected=!produit.selected;
    return this.http.put<ProductModel>(host+"/produit/"+produit.id, produit);
  }

  deleteProduct(product: ProductModel):Observable<void>{
    let host = environment.host;
    return this.http.delete<void>(host+"/produit/delete/"+product.id);
  }

  saveProduct(product: ProductModel):Observable<ProductModel>{
    let host = environment.host;
    return this.http.post<ProductModel>(host+"/produit/add", product);
  }

  getProduct(id:number):Observable<ProductModel>{
    let host = environment.host;
    return this.http.get<ProductModel>(host+"/produit/find/"+id);
  }

  updateProduct(p:ProductModel):Observable<ProductModel>{
    let host = environment.host;
    return this.http.put<ProductModel>(host+"/produit/update/"+p.id, p);

  }


}

