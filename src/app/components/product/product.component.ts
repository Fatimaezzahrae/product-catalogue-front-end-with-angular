import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map, Observable, of, startWith } from 'rxjs';
import { ProductModel } from 'src/app/model/product.model';
import { ProductService } from 'src/app/services/product.service';
import { AppDataState, DataStateEnum } from 'src/app/State/product.state';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {


  products$ : Observable<AppDataState<ProductModel[]>> | null=null;
  readonly DataStateEnum=DataStateEnum;

  constructor(private productService: ProductService, private router:Router) { }

  ngOnInit(): void {
  }

  onGetAllProducts(){
    this.products$= this.productService.getAllProducts().pipe(
      map(data=>{
        console.log(data);
        return ({dataState:DataStateEnum.LOADED, data:data})
      }),
      startWith({dataState:DataStateEnum.LOADING}),
      catchError(err=>of({dataState:DataStateEnum.ERROR, errorMessage:err.message}))
    );
  }

  onGetSelectedProducts(){
    this.products$=this.productService.getSelectedProducts().pipe(
      map(data=>{
        console.log(data);
        return ({dataState:DataStateEnum.LOADED, data:data})
      }),
      startWith({dataState:DataStateEnum.LOADING}),
      catchError(err=>of({dataState:DataStateEnum.ERROR, errorMessage:err.message}))
    );
  }

  onGetAvailableProducts(){
    this.products$=this.productService.getAvailabledProducts().pipe(
      map(data=>{
        console.log(data);
        return ({dataState:DataStateEnum.LOADED, data:data})
      }),
      startWith({dataState:DataStateEnum.LOADING}),
      catchError(err=>of({dataState:DataStateEnum.ERROR, errorMessage:err.message}))
    );
  }

  onSearchProducts(dataForm: any){
    this.products$=this.productService.searchProducts(dataForm.keyword).pipe(
      map(data=>{
        console.log(data);
        return ({dataState:DataStateEnum.LOADED, data:data})
      }),
      startWith({dataState:DataStateEnum.LOADING}),
      catchError(err=>of({dataState:DataStateEnum.ERROR, errorMessage:err.message}))
    );
  }

  onSelectProduct(p:ProductModel){
    this.productService.select(p).subscribe(data=>{
      p.selected=data.selected;
    })
  }

  onDeleteProduct(p:ProductModel){
    let v= confirm("Etes vous sûr de vouloir supprimer")
    if(v===true)
    this.productService.deleteProduct(p).subscribe(data=>{
      this.onGetAllProducts();
    })

  }

  onNewProduct(){
    this.router.navigateByUrl("/newProduct");
  }

  onEditProduct(p: ProductModel){
    this.router.navigateByUrl("/editProduct/"+p.id);
  }



}
