import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductModel } from 'src/app/model/product.model';
import { ProductService } from 'src/app/services/product.service';


@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  productFormGroup: FormGroup = new FormGroup({
    name: new FormControl(''),
    price: new FormControl(''),
    quantity: new FormControl(''),
    selected: new FormControl(''),
    available: new FormControl(''),
  });
  submitted:boolean=false;

  constructor(private fb:FormBuilder, private productService: ProductService) { }

  ngOnInit(): void {
    this.productFormGroup = this.fb.group({
      name: ["", Validators.required],
      price: [0, Validators.required],
      quantity: [0, Validators.required],
      selected: [true, Validators.required],
      available: [true, Validators.required],
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.productFormGroup.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.productFormGroup.invalid) {
      return;
    }

    console.log(JSON.stringify(this.productFormGroup.value, null, 2));
  }

  onReset(): void {
    this.submitted = false;
    this.productFormGroup.reset();
  }
  
  onSaveProduct(){
    this.submitted=true;
    if(this.productFormGroup?.invalid)return;
    this.productService.saveProduct(this.productFormGroup?.value).subscribe(data=>{
      alert("Success Saving product")
    });
  }
  

}
