import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
  productId : number;
  productFormGroup: FormGroup = new FormGroup({
    name: new FormControl(''),
    price: new FormControl(''),
    quantity: new FormControl(''),
    selected: new FormControl(''),
    available: new FormControl(''),
  });
  submitted:boolean=false;

  constructor(private productService:ProductService, 
    private activatedRoute:ActivatedRoute,
    private fb: FormBuilder) { 
    this.productId=activatedRoute.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.productService.getProduct(this.productId).subscribe(product=>{
      this.productFormGroup = this.fb.group({
        id:[product.id, Validators.required],
        name:[product.name, Validators.required],
        price:[product.price, Validators.required],
        quantity:[product.quantity, Validators.required],
        selected:[product.selected, Validators.required],
        available:[product.available, Validators.required],     
       })
    })
  }

  get f(): { [key: string]: AbstractControl } {
    return this.productFormGroup.controls;
  }

  onUpdateProduct(){
    this.productService.updateProduct(this.productFormGroup?.value).subscribe(data=>{
      alert("success ! the product is added yo !")
    });
  }

  
}
